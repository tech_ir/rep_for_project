<?php

declare(strict_types=1);

namespace App\Service;

use LogicException;

class CalculatorServiceTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @test
     */
    public function sum7plus8(): void
    {
        $service = new CalculatorService();

        self::assertEquals(15, $service->sum(7, 8));
    }

    /**
     * @test
     */
    public function div4by2(): void
    {
        $service = new CalculatorService();

        $expected = 2;
        $actual = $service->div(4, 2);

        self::assertEquals($expected, $actual);
    }

    /**
     * @test
     */
    public function div4by8(): void
    {
        $service = new CalculatorService();

        $this->expectException(LogicException::class);

        $service->div(4, 0);

    }
}