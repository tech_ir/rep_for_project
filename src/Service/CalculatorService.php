<?php

declare(strict_types=1);

namespace App\Service;

use LogicException;

class CalculatorService{
    public function sum(float $a, float $b): float
    {
        return $a + $b;
    }

    public function div(float $a, float $b): float
    {
        if($b == 0){
            throw new LogicException("zero is tried to be divided");
        }
        return $a / $b;
    }
}